# Gitlab

## Subir gitlab no ambiente local

### CPU

4 cores / 4Gb para 500 users
8 cores / 8Gb  para 1000 users

### Gitlab Ranner

1 vCPU
3.75 Gb Ram

### Instalar Gitlab

```bash
```

### Gitlab CI

```yaml
# .gitlab-ci.yml
image: docker:19.03

stages:
  - build
  - test
  - push
  - deploy

variables:
  docker_version: "19.03"

before_script:
  - git clone https://gitlab.com/pluralsis-devops/descomplicandokubernetes.git

build-app:
  stage: build
  script:
    - docker image build -t giropops:1.0 .

test-app:
  stage: test
  only:
   - branches
  except:
    - main
  script:
    - docker conatiner run -it --rm giropops:1.0 /script/test.sh

push-image-app:
  stage: push
  only:
   - branches
  except:
    - main
  script:
    - docker image push giropops:1.0

deploy-app:
  stage: deploy
  only:
   - branches
  except:
    - main
  script:
    - kubectl apply -f deployment.yaml
```
